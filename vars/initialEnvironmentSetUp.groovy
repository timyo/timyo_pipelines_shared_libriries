def call(String environmentVariable){
    script {
        try {
            if (environmentVariable == "empty") {
                echo ""
            }
        } catch (e) {
            currentBuild.result = 'ABORTED'
            error('That was the first dry build of the branch. Please, replay the pipeline.')
        }
    }
}