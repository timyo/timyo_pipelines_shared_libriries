def call(Map config){
    def buildResult = ""
    def testUrl = ""
    def artifactPattern = config?.artifactPattern ?: ""
    def projectUnderTest = config?.projectUnderTest ?: "${env.JOB_NAME}"
    def additionalTestParams = config?.additionalTestParams ?: ""
    def testFailThreshold = config?.testFailThreshold ?: 1
    def testNewFailThreshold = config?.testNewFailThreshold ?: ""
    def retries = config?.numberOfRetries ?: 1
    def buildPermalink = config?.buildPermalink ?: "lastBuild"
    def retryCount = 1

    retry(retries) {
        buildResult = build(
                job: config.autoTestJob,
                parameters: [
                        [$class: 'StringParameterValue', name: 'projectUnderTest', value: "$projectUnderTest"],
                        [$class: 'StringParameterValue', name: 'testSuite', value: "$config.testSuite"],
                        [$class: 'StringParameterValue', name: 'timyoEnvironment', value: "$config.timyoEnvironment"],
                        [$class: 'StringParameterValue', name: 'artifactPattern', value: "$artifactPattern"],
                        [$class: 'StringParameterValue', name: 'buildPermalink', value: "$buildPermalink"],
                        [$class: 'StringParameterValue', name: 'testFailThreshold', value: "$testFailThreshold"],
                        [$class: 'StringParameterValue', name: 'testNewFailThreshold', value: "$testNewFailThreshold"],
                        [$class: 'StringParameterValue', name: 'additionalTestParams', value: "$additionalTestParams"],
                ],
                propagate: false)
        testUrl = buildResult.absoluteUrl
        echo "Report: ${testUrl}allure"
        if(retryCount != retries && buildResult.result == 'FAILURE'){
            retryCount++
            sh "This attempt has failed. Trying once again.."
        }
    }
    return [buildResult, testUrl]
}