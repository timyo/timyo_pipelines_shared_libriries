def call(String result) {
    if (result == 'SUCCESS') {
        echo "Tests have passed successfully"
    }

    if (result == 'UNSTABLE') {
        echo "Tests have passed, but there are failures"
    }

    if (result == 'FAILURE') {
        sh "Tests have failed. Check the report for results"
    }
}