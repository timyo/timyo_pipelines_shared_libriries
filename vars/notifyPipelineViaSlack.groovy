import net.sf.json.JSONArray
import net.sf.json.JSONObject

this.class.classLoader.parseClass("vars/getBuildStatus.groovy")

def call(Map config) {
    // buildStatus of null means successful
    def buildStatus = getBuildStatus() ?: 'SUCCESS'
    def isMultibranch = config.get('isMultibranch', true)

    // Default values
    def hostUrl = env.BUILD_URL.split('/')[2].split(':')[0]

    if (isMultibranch) {
        artifactsLink = "https://${hostUrl}/blue/organizations/jenkins/${env.JOB_NAME.substring(0, env.JOB_NAME.indexOf("/"))}/detail/${env.BRANCH_NAME}/${env.BUILD_NUMBER}/artifacts"
    } else {
        artifactsLink = "https://${hostUrl}/blue/organizations/jenkins/${env.JOB_NAME}/detail/${env.JOB_NAME}/${env.BUILD_NUMBER}/artifacts"

    }
    def subject = "${buildStatus}! Took: ${currentBuild.durationString}.\nJob: *${env.JOB_NAME}*  (<${env.RUN_DISPLAY_URL}|*Open*>) (<${env.RUN_CHANGES_DISPLAY_URL}|  *Changes*>) (<$artifactsLink|  *Artifacts*>)"
    def title = "${env.JOB_NAME} Build: ${env.BUILD_NUMBER}"
    def title_link = "${env.RUN_DISPLAY_URL}"
    def branchName = "${env.BRANCH_NAME}"
    def sonarQubeProjectLink = "(<https://sonar.timyo.com/project/issues?branch=${env.BRANCH_NAME}&id=${config?.sonarProjectKey ?: ''}|*Sonar Result*>)"

    if (isUnix()) {
        commitAuthor = sh(returnStdout: true, script: "git --no-pager show -s --format='%an'").trim()
    } else {
        commitAuthor = powershell(returnStdout: true, script: "git --no-pager show -s --format='%an'").trim()
    }
    echo "Commit's author name is $commitAuthor"
    //Extract slack id from author name if exists
    def slackIdIndex = commitAuthor.indexOf("|")
    if (slackIdIndex > 0) {
        author = "<${commitAuthor.substring(slackIdIndex + 1).trim()}>"
    } else {
        author = commitAuthor
    }
    // get commit messages from last successful build
    def commitMessages = getCommitMessage(currentBuild)

    // get unit test results for slack message
    def unitTestsSummary = "`${getTestSummary(currentBuild)}`"

    //get automation tests results
    def autoTestsResults = config?.autoTestsResults ?: []
    def autoTestsSummary = ""
    if (autoTestsResults.size() > 0) {
        for (int i = 0; i < autoTestsResults.size(); i++) {
            autoTestsSummary = autoTestsSummary + "${autoTestsResults[i]} \n"
        }
    } else {
        autoTestsSummary = "`No tests found`"
    }

    // Set quality gate output
    try {
        if (config?.qualityGateStatus.status == "OK") {
            qualityGateResult = "Passed :heavy_check_mark: $sonarQubeProjectLink"
        } else {
            qualityGateResult = "Fail :x: $sonarQubeProjectLink"
        }
    } catch (ex) {
        qualityGateResult = "Skipped :double_vertical_bar:"
    }

    def statusIcon = ""
    // Override default values based on build status
    if (buildStatus == 'STARTED') {
        color = 'YELLOW'
        colorCode = '#FFFF00'
    } else if (buildStatus == 'SUCCESS') {
        color = 'GREEN'
        colorCode = 'good'
        statusIcon = "https://cdn3.iconfinder.com/data/icons/security-2-1/512/thumb_up-512.png"
    } else if (buildStatus == 'UNSTABLE') {
        color = 'YELLOW'
        colorCode = 'warning'
        statusIcon = "https://ih1.redbubble.net/image.524291195.6690/flat,550x550,075,f.u2.jpg"
    } else if (buildStatus == 'ABORTED') {
        color = 'GREY'
        colorCode = '#919187'
        statusIcon = "https://cdn4.iconfinder.com/data/icons/flat-pro-multimedia-set-1/32/btn-grey-play-stop-512.png"
    } else {
        color = 'RED'
        colorCode = 'danger'
        statusIcon = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTz0Vk09-wWHchkoAHfnExfT1ed3sV_sFtjv-KE-N4kNxPC1Q5o"
    }

    JSONObject attachment = new JSONObject()
    attachment.put('author', "jenkins")
    attachment.put('author_link', "https://jenkins.timyo.com/blue/")
    attachment.put('title', title.toString())
    attachment.put('title_link', title_link.toString())
    attachment.put('text', subject.toString())
    attachment.put('fallback', "fallback message")
    attachment.put('color', colorCode)
    attachment.put('mrkdwn_in', ["fields", "text"])

    // JSONObject for branch
    JSONObject branch = new JSONObject()
    branch.put('title', 'Branch:')
    branch.put('value', branchName.toString())
    branch.put('short', true)

    // JSONObject for author
    JSONObject commitAuthor = new JSONObject()
    commitAuthor.put('title', 'Author:')
    commitAuthor.put('value', author.toString())
    commitAuthor.put('short', true)
    if (buildStatus == 'SUCCESS') {
        // JSONObject for additional info
        def additionalInfoValue = config?.additionalNotify ?: ''
        JSONObject additionalInfo = new JSONObject()
        additionalInfo.put('title', 'Additional info:')
        additionalInfo.put('value', additionalInfoValue.toString())
        additionalInfo.put('short', false)

        // JSONObject for branch
        JSONObject commitMessage = new JSONObject()
        commitMessage.put('title', 'Commit Messages:')
        commitMessage.put('value', commitMessages.toString())
        commitMessage.put('short', false)

        // JSONObject for branch
        JSONObject qualityGates = new JSONObject()
        qualityGates.put('title', 'Quality Gates:')
        qualityGates.put('value', qualityGateResult.toString())
        qualityGates.put('short', false)

        // JSONObject for test results
        JSONObject testResults = new JSONObject()
        testResults.put('title', 'Unit Tests: ' + unitTestsSummary.toString())
        testResults.put('short', false)

        // JSONObject for test results
        JSONObject automationTestResults = new JSONObject()
        automationTestResults.put('title', 'Automation Tests:')
        automationTestResults.put('value', autoTestsSummary.toString())
        automationTestResults.put('short', false)

        attachment.put('fields', [branch, commitAuthor, additionalInfo ?: [], commitMessage, qualityGates, testResults, automationTestResults])
    }
    attachment.put('image_url', "")
    attachment.put('thumb_url', statusIcon.toString())
    attachment.put('footer', "Timyo Pipeline")
    attachment.put('footer_icon', "https://emoji.slack-edge.com/T1SJMBXE1/timyo/de1f602f167a9e92.png")
    JSONArray attachments = new JSONArray()
    attachments.add(attachment)

    println attachments.toString()

    // Send notifications
    slackSend(color: colorCode, message: subject, attachments: attachments.toString(), channel: config.channel)
}

def getCommitMessage(build) {
    def changeLogSets = build.changeSets
    def messages = ""
    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            if (!messages.contains("${entry.msg}\n")) {
                messages = "$messages${entry.msg}\n"
            }
        }
    }
    return messages
}