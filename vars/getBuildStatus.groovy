def call() {
    def interruptedAction = currentBuild.rawBuild.getActions(jenkins.model.InterruptedBuildAction.class)
    if (interruptedAction.isEmpty()) {
        return currentBuild.currentResult
    } else {
        return 'ABORTED'
    }
}