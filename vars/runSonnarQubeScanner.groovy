def call(Map config) {
    withSonarQubeEnv('Timyo Sonar') {
        script {
            def scannerHome = tool "SonarQube 3.2.0"
            sh "${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=$config.sonarProjectKey " +
                    "-Dsonar.projectName='$config.sonarProjectName' " +
                    "-Dsonar.projectVersion='$config.projectVersion' " +
                    "-Dsonar.sources=${config.sonarSources ?: '.'} " +
                    "-Dsonar.java.binaries=${config.sonarJavaBinaries ?: ''} " +
                    "-Dsonar.tests=${config.sonarTests ?: ''} " +
                    "-Dsonar.cfamily.build-wrapper-output=${config.sonarCfamilyBuildWrapperOutput ?: ''} " +
                    "-Dsonar.cfamily.threads=${config.sonarCfamilyThreads ?: ''} " +
                    "-Dsonar.objectivec.workspace=${config.sonarObjectivecWorkspace ?: ''} " +
                    "-Dsonar.objectivec.appScheme=${config.sonarObjectivecAppScheme ?: ''} " +
                    "-Dsonar.objectivec.excludedPathsFromCoverage=${config.sonarObjectivecExcludedPathsFromCoverage ?: ''} " +
                    "-Dsonar.modules=${config.sonarModules ?: ''} " +
                    "-Dapi.sonar.projectName=${config.apiSonarProject ?: ''} " +
                    "-Dcore.sonar.projectName=${config.coreSonarProject ?: ''} " +
                    "-Dcore.sonar.projectName=${config.statModelProject ?: ''} " +
                    "-Dcore.sonar.projectName=${config.statServerProject ?: ''} " +
                    "-Dsonar.branch.name=${env.BRANCH_NAME} " +
                    "-Dsonar.java.source=${config.sonarJavaSource ?: ''} "
        }
    }
}