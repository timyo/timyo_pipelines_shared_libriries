##Project for storing Jenkins Pipelines Shared Libraries

All methods accessible in Jenkins pipelines via the name of file in `vars` folder e.g. `runSonnarQubeScanner` with parameters.

To apply changes in existing methods or adding a new ones you need to push your changes to master branch of this repository.

###Settings 
All connection settings can be find on Jenkins in *Jenkins -> Manage Jenkins -> Configure System -> Global Pipeline Libraries*